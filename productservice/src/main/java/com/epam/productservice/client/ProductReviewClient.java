package com.epam.productservice.client;

import com.epam.productservice.ProductReviewClientConfig;
import com.epam.productservice.entity.Review;
import com.epam.productservice.fallback.ProductReviewFallback;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "review-service",
        configuration = ProductReviewClientConfig.class,
        fallback = ProductReviewFallback.class)
@RibbonClient(name="review-service")
public interface ProductReviewClient {

    @GetMapping(value = "/api/{productId}/reviews")
    List<Review> getByProductId(@PathVariable Long productId);

    @PostMapping(value="/api/{productId}/reviews")
    public Review addProductReview(@PathVariable int productId,@RequestBody Review review);

    @PutMapping(value="/api/{productId}/reviews/{reviewId}")
    public Review updateProductReview( @PathVariable int productId, @PathVariable int reviewId,@RequestBody Review review);

    @DeleteMapping(value="/api/{productId}/reviews/{reviewId}")
    public ResponseEntity<Object> deleteProductReview(@PathVariable int productId,@PathVariable int reviewId);
}
