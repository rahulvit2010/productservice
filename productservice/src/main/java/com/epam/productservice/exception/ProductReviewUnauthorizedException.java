package com.epam.productservice.exception;

public class ProductReviewUnauthorizedException extends Exception {
    public ProductReviewUnauthorizedException(String reason) {

            super(reason);

    }
}
