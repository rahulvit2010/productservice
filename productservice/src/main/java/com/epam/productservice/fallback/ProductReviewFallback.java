package com.epam.productservice.fallback;

import com.epam.productservice.client.ProductReviewClient;
import com.epam.productservice.entity.Review;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class ProductReviewFallback implements ProductReviewClient {
    @Override
    public List<Review> getByProductId(Long productId) {
         return Collections.emptyList();
    }

    @Override
    public Review addProductReview(int productId, Review review) {
        return new Review();
    }

    @Override
    public Review updateProductReview(int productId, int reviewId, Review review) {
        return new Review();
    }

    @Override
    public ResponseEntity<Object> deleteProductReview(int productId, int reviewId) {
        return new ResponseEntity<>(null);
    }
}
