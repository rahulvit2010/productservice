package com.epam.productservice.service;

import com.epam.productservice.client.ProductReviewClient;
import com.epam.productservice.entity.Review;
import com.epam.productservice.exception.handler.ProductResponseErrorHandler;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {

    private RestTemplate restTemplate;

    private EurekaClient eurekaClient;

    private HttpHeaders httpHeaders;

    private  HttpEntity httpEntity;

    @Autowired
    ProductReviewClient productReviewClient;

    @Value("${target.service-name}")
    private String serviceName ;

    public ProductReviewServiceImpl(EurekaClient eurekaClient) {
        this.restTemplate = new RestTemplate();
        this.eurekaClient = eurekaClient;
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    }

    @Override
    public List<Review> getReviews(int productId) {
        createHttpEntity(null);
        return restTemplate.exchange(getServiceUrl() + "/api/" +productId+"/reviews"
                , HttpMethod.GET
                ,  this.httpEntity
                , new ParameterizedTypeReference<List<Review>>() {
                }).getBody();

    }

    @Override
    public Review addProductReview(Review review, int productId) {
        /*createHttpEntity(review);
        return restTemplate.exchange(getServiceUrl() +"/api/"+productId+"/reviews/"
                , HttpMethod.POST
                ,  this.httpEntity
                , new ParameterizedTypeReference<Review>()
                {}).getBody();*/
        return productReviewClient.addProductReview(productId,review);
    }

    @Override
    public Review updateProductReview(Review review, int productId, int reviewId) {
        /*createHttpEntity(review);
        return restTemplate.exchange(getServiceUrl() +"/api/"+productId+"/reviews/"+ reviewId
                , HttpMethod.PUT
                ,  this.httpEntity
                , new ParameterizedTypeReference<Review>()
                {}).getBody();*/

        return productReviewClient.updateProductReview(productId,reviewId,review);
    }

    @Override
    public ResponseEntity<Object> deleteProductReview(int productId, int reviewId) {
        /*createHttpEntity(null);
        restTemplate.setErrorHandler(new ProductResponseErrorHandler());
         return  restTemplate.exchange( getServiceUrl() +"/api/"+productId+"/reviews/"+ reviewId
                , HttpMethod.DELETE
                ,  httpEntity
                , Object.class
                );*/
       return productReviewClient.deleteProductReview(productId,reviewId);
    }

    public void createHttpEntity(Review o) {
        httpEntity = new HttpEntity(o, this.httpHeaders);
    }

    private String getServiceUrl() {
        InstanceInfo instance = eurekaClient.getNextServerFromEureka(serviceName, false);
        return instance.getHomePageUrl();
    }
}
