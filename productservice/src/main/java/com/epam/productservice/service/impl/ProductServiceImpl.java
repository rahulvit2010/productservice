package com.epam.productservice.service.impl;

import com.epam.productservice.client.ProductReviewClient;
import com.epam.productservice.dao.ProductCategoryRepository;
import com.epam.productservice.dao.ProductRepository;
import com.epam.productservice.entity.Product;
import com.epam.productservice.entity.ProductCategory;
import com.epam.productservice.entity.Review;
import com.epam.productservice.exception.NoContentFoundException;
import com.epam.productservice.exception.ProductCategoryNotFound;
import com.epam.productservice.exception.ProductNotFoundException;
import com.epam.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Autowired
    ProductReviewClient productReviewClient;

    @Override
    public List<Product> retreiveAll()
    {
        List<Product> products= Optional.ofNullable(productRepository.findAll())
                .orElseThrow(
                ()-> new NoContentFoundException("Sorry, there are no records found"));

        products.stream()
                .forEach(product -> product.addReviews
                        (productReviewClient.getByProductId(product.getProductId())));
        return products;
    }

    @Override
    public Product retreiveById(Long productId) {

        return productRepository.findById(productId)
                .map(product->product.addReviews(productReviewClient.getByProductId(productId)))
                .orElseThrow(() -> new ProductNotFoundException("Product not found with id " + productId));


    }

    @Override
    public Product save(Product product) {
            checkExistence(product.getProductCategory().getId());
            productRepository.save(product);
            return productRepository.findById(product.getProductId()).orElseThrow(
                    ()-> new RuntimeException("product is not saved in database")
            );


    }

    @Override
    public void deleteById(Long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public boolean checkExistence(int id) {
        return productCategoryRepository.findById(id)
                .map( productCategory  -> true)
                .orElseThrow(
                        ()-> new ProductCategoryNotFound("product category is not valid"));
    }
}
